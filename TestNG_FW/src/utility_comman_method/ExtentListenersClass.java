package utility_comman_method;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenersClass implements ITestListener {
	 
	ExtentSparkReporter sparkreport;
	ExtentReports extentreport;
	ExtentTest test;
	
	public  void report_configuration() {
		sparkreport=new ExtentSparkReporter(".\\extent-report\\report.html");
		extentreport=new ExtentReports();
		extentreport.attachReporter(sparkreport); 
		//add system/environment info
		extentreport.setSystemInfo("OS", "Windows 11"); 
		extentreport.setSystemInfo("USer", "manisha saini");
		
		//configure the report feel and look
		sparkreport.config().setDocumentTitle("PROJECT 2 REPORT");
		sparkreport.config().setReportName("TEST 1 SIT REPORT");
		sparkreport.config().setTheme(Theme.DARK);
	}
	
	@Override
	public void onStart(ITestContext context) {
		System.out.println("staret method invoke..");
		report_configuration();
	}
	
	@Override
	public void onFinish(ITestContext context) {
		System.out.println("finish method invoke..");
		extentreport.flush();
	}
	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Name of the method failed: "+result.getName());
		test=extentreport.createTest(result.getName());
		test.log(Status.FAIL, MarkupHelper.createLabel("Name of the test case failed: "+result.getName(), ExtentColor.RED));
	}
	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("Name of the method skipped: "+result.getName());
		test=extentreport.createTest(result.getName());
		test.log(Status.SKIP, MarkupHelper.createLabel("Name of the test case skipped: "+result.getName(), ExtentColor.YELLOW));
	}
	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("Name of the test method started: "+result.getName());
	}
	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("name of the method pass:"+result.getName());
		test=extentreport.createTest(result.getName());
		test.log(Status.PASS, MarkupHelper.createLabel("Name of the test case pass"+result.getName(), ExtentColor.GREEN)); 
	}
	
                      
}
