package validation_package;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import api_common_methods.Common_method_handle_API;
import endpoints.post1_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.post_new_request;

public class post_tc {
	@Test
	public static void executor() throws IOException {
		
		String endpoint=post1_endpoint.endpoint_post();
		String requestBody=post_new_request.requestBody_post();
		System.out.println(requestBody); 
		
		String responseBody=Common_method_handle_API.post_responseBody(requestBody, endpoint);
		System.out.println(responseBody); 
		
		JsonPath req=new JsonPath(requestBody);
		String req_email=req.getString("email");
		
		JsonPath res=new JsonPath(responseBody);
		String res_error=res.getString("error");
		
		Assert.assertNotNull(req_email);
		Assert.assertNotNull(res_error);

	}


}
