package validation_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import api_common_methods.Common_method_handle_API;
import endpoints.Patch_endpoints;
import io.restassured.path.json.JsonPath;
import request_repository.Patch_request_repository;
import utility_comman_method.handle_API_logs;
import utility_comman_method.handle_directory;

public class Patch_validation extends Common_method_handle_API {
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;
	
	@BeforeTest
	public static void test_setup() throws IOException {
		 log_dir=handle_directory.create_log_directory("Patch_validation_logs");
		 requestBody = Patch_request_repository.patch_request_tc1();
		 endpoint = Patch_endpoints.patch_endpoint_tc1();
	}
	
	@Test(description="::::::::::::executed patch API :::::::::::::::")
	public static void patch_executor() throws IOException {
		

		int statusCode = patch_statusCode(requestBody, endpoint);
		System.out.println(statusCode);

		for (int i = 0; i < 5; i++) {
			if (statusCode == 200) {
				String responseBody = patch_responseBody(requestBody, endpoint);
				System.out.println("Request successfull!!");
				System.out.println(responseBody);
				Patch_validation.validator(responseBody, requestBody); 
				break;
			} else {
				System.out.println("Expected status code not recevied!!RETRYING...");
			}
		}

	}
	public static void validator(String responseBody, String requestBody) {
	JsonPath jsp_req= new JsonPath(requestBody);
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		LocalDateTime current_date=LocalDateTime.now();
		String exp_date=current_date.toString().substring(0,11);
		
	JsonPath jsp_res= new JsonPath(responseBody);
		String res_name=jsp_res.getString("name");
		String res_job=jsp_res.getString("job");
		String res_date=jsp_res.getString("updatedAt");
		res_date=res_date.substring(0,11);
		//validate response body
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, exp_date);
	}
	@AfterTest
   public static void test_tearDown() throws IOException {
		handle_API_logs.evidence_creator(log_dir, "patch_validation", endpoint, requestBody, responseBody);

   }
}
