package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_comman_method.execl_data_extractor;

public class Put_request_repository {
	public static String put_repository_tc1() throws IOException {
		ArrayList<String> data=execl_data_extractor.Excel_data_reader("test_data", "put_API", "put_TC1");
		String name=data.get(1);
		String job=data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
        return requestBody;
	}

}
