package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import utility_comman_method.execl_data_extractor;

public class Patch_request_repository {
	public static String patch_request_tc1() throws IOException {
		ArrayList<String> data= execl_data_extractor.Excel_data_reader("test_data", "patch_API", "patch_TC1");
		String name=data.get(1);
		String job=data.get(2);
		
		
		String requestBody = "{\r\n" + "    \"name\": \""+name+"\",\r\n" + "    \"job\": \""+job+"\"\r\n" + "}";
        return requestBody;
	}

}
