package request_repository;

import org.testng.annotations.DataProvider;

public class Testng_data_provider {
	@DataProvider()
	public Object[][] post_requestbody(){
	return new Object[][]{
		{"manisha","QA"},
		{"uma","TL"},
		{"swati","manager"}
	};
	}	

}
